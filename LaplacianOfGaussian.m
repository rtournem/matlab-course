function value = LaplacianOfGaussian(x, y, sigma)
value=1/(pi*sigma^4).*(1-(x.^2+y.^2)./(2*sigma^2)).*exp(-(x.^2+y.^2)./(2*sigma^2));