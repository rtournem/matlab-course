function Partie3()
%dur�e unitaire
t = 0.17;
%intro
% Key{1} = [ 56 56 0 56 0 52 56 0 59 0 0 47 0 0 ];
% Dur{1} = [t t t t t t t t t t 2*t t t 2*t ];
% Key{2} = [ 30 30 0 30 0 30 30 0 47 0 0 35 0 0];
% Dur{2} = [ t t t t t t t t t t 2*t t t 2*t];
% Key{3}= [ 46 46 0 46 0 46 46 0 51 0 0 47 0 0 ];
% Dur{3} = [ t t t t t t t t t t 2*t t t 2*t];

%morceau entier
Key{1} = [  56 56 0 56 0 52 56 0 59 0 0 47 0 0                         52 0 47 0 44 0 0 49 0 51 0 50 49 0 47 0 56 0 59 0 61 0 57 59 0 56 0 52 54 51 0      52 0 47 0 44 0 0 49 0 51 0 50 49 0 47 0 56 0 59 0 61 0 57 59 0 56 0 52 54 51 0                   0 59 58 57 55 0 56 0 48 49 52 0 49 52 54 0 59 58 57 55 0 56 0 64 0 64 64 0 0 0 59 58 57 55 0 56 0 48 49 52 0 49 52 54 0 55 0 0 54 0 52 0 0 0           0 59 58 57 55 0 56 0 48 49 52 0 49 52 54 0 59 58 57 55 0 56 0 64 0 64 64 0 0 0 59 58 57 55 0 56 0 48 49 52 0 49 52 54 0 55 0 0 54 0 52 0 0 0                 52 52 0 52 0 52 54 0 56 52 0 49 47 0 0 52 52 0 52 0 52 54 56 0 0 52 52 0 52 0 52 54 0 56 52 0 49 47 0 0          56 56 0 56 0 52 56 0 59 0 0 47 0 0          56 52 0 47 0 48 0 49 57 0 57 49 0 0 51 0 61 0 61 0 61 0 59 0 57 0 56 52 0 49 47 0 0 56 52 0 47 0 48 0 49 57 0 57 49 0 0 47 57 0 57 57 0 56 0 54 0 52 0 0 0 52 0 47 0 44 0 49 51 49 48 50 48 47];
Dur{1} = [  t t t t t t t t t t 2*t t t 2*t             t 2*t t 2*t t t t t t t t t t t  (2/3)*t  (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t      t 2*t t 2*t t t t t t t t t t t  (2/3)*t  (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t             2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t 2*t 2*t t t t t t t t t t t t t t t 2*t t t t t 2*t t t 2*t 4*t              2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t 2*t 2*t t t t t t t t t t t t t t t 2*t t t t t 2*t t t 2*t 4*t               t t t t t t t t t t t t t t 2*t t t t t t t t t 4*t 4*t t t t t t t t t t t t t t t 2*t                     t t t t t t t t t t 2*t t t 2*t               t t t t 2*t t t t t t t t t 2*t  (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t 2*t t t t t 2*t t t t t t t t t 2*t t t t t  (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t 2*t 4*t        t 2*t t 2*t t t (4/3)*t (4/3)*t (4/3)*t  (4/3)*t (4/3)*t (4/3)*t (8*t) ];
Key{2} = [   30 30 0 30 0 30 30 0 47 0 0 35 0 0         35 0 32 0 28 0 0 33 0 35 0 34 33 0 32 0 40 0 44 0 45 0 42 44 0 40 0 37 39 35 0          35 0 32 0 28 0 0 33 0 35 0 34 33 0 32 0 40 0 44 0 45 0 42 44 0 40 0 37 39 35 0              28 0 35 0 40 0 33 0 40 40 40 33 0 28 0 32 0 35 40 0 57 0 57 57 0 35 0 28 0 35 0 40 0 33 0 40 40 40 33 0 0 36 0 0 38 0 40 0 35 35 0 28 0               28 0 35 0 40 0 33 0 40 40 40 33 0 28 0 32 0 35 40 0 57 0 57 57 0 35 0 28 0 35 0 40 0 33 0 40 40 40 33 0 0 36 0 0 38 0 40 0 35 35 0 28 0              24 0 31 0 36 0 35 0 28 0 23 0 24 0 31 0 36 0 35 0 28 0 23 0 24 0 31  0 36 0 35 0 28 0 23 0          30 30 0 30 0 30 30 0 47 0 0 35 0 0                28 0 34 35 0 40 0 33 0 33 0 40 40 33 0 30 0 33 35 0 39 0 35 0 35 0 40 40 35 0 28 0 34 35 0 40 0 33 0 33 0 40 40 33 0 35 0 35 35 0 37 0 39 0 40 0 35 0 28 0 0         35 0 32 0 28 0 33 29 28                ];
Dur{2} = [  t t t t t t t t t t 2*t t t 2*t           t 2*t t 2*t t t t t t t t t t t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t          t 2*t t 2*t t t t t t t t t t t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t       t 2*t t 2*t t t t 2*t t t t t t t 2*t t 2*t t t t t t t t t t t t 2*t t 2*t t t t 2*t t t t t t 2*t t t t t 2*t t 2*t t t t t t          t 2*t t 2*t t t t 2*t t t t t t t 2*t t 2*t t t t t t t t t t t t 2*t t 2*t t t t 2*t t t t t t 2*t t t t t 2*t t 2*t t t t t t              t 2*t t 2*t t t t 2*t t 2*t t t t 2*t t 2*t t t t 2*t t 2*t t t t 2*t t 2*t t t  t 2*t t 2*t t t         t t t t t t t t t t 2*t t t 2*t             t 2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t t t 2*t t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t 2*t         t 2*t t 2*t t t 4*t 4*t 8*t  ];
Key{3} = [ 46 46 0 46 0 46 46 0 51 0 0 47 0 0          44 0 40 0 35 0 0 40 0 42 0 41 40 0 40 0 47 0 51 0 52 0 49 51 0 49 0 56 45 42 0          44 0 40 0 35 0 0 40 0 42 0 41 40 0 40 0 47 0 51 0 52 0 49 51 0 49 0 56 45 42 0            0 56 55 54 51 0 52 0 44 45 47 0 40 44 45 0 56 55 54 51 0 52 0 59 0 59 59 0 0 0 56 55 54 51 0 52 0 44 45 47 0 40 44 45 0 48 0 0 45 0 44 0 0 0            0 56 55 54 51 0 52 0 44 45 47 0 40 44 45 0 56 55 54 51 0 52 0 59 0 59 59 0 0 0 56 55 54 51 0 52 0 44 45 47 0 40 44 45 0 48 0 0 45 0 44 0 0 0              48 48 0 48 0 48 50 0 47 44 0 44 40 0 0 48 48 0 48 0 48 50 47 0 0 48 48 0 48 0 48 50 0 47 44 0 44 40 0 0          46 46 0 46 0 46 46 0 51 0 0 47 0 0           52 49 0 44 0 44 0 45 52 0 52 45 0 0 47 0 57 0 57 0 57 0 56 0 54 0 52 49 0 45 44 0 0 52 49 0 44 0 44 0 45 52 0 52 45 0 0 47 54 0 54 54 0 52 0 51 0 47 44 0 44 40 0 0           44 0 40 0 35 0 45 45 44 42 44                 ];
Dur{3} = [  t t t t t t t t t t 2*t t t 2*t        t 2*t t 2*t t t t t t t t t t t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t               t 2*t t 2*t t t t t t t t t t t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t t t t t 2*t            2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t 2*t 2*t t t t t t t t t t t t t t t 2*t t t t t 2*t t t 2*t 4*t                2*t t t t t t t t t t t t t t t 2*t t t t t t t t t t t t t 2*t 2*t t t t t t t t t t t t t t t 2*t t t t t 2*t t t 2*t 4*t          t t t t t t t t t t t t t t 2*t t t t t t t t t 4*t 4*t t t t t t t t t t t t t t t 2*t        t t t t t t t t t t 2*t t t 2*t               t t t t 2*t t t t t t t t t 2*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t 2*t t t t t 2*t t t t t t t t t 2*t t t t t  (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t (2/3)*t t t t t t t 2*t          t 2*t t 2*t t t 4*t 4*t t t 6*t              ];

%formule pour passer de Key � la fr�quence: freq=440*2^((Key-49)/12);
fs=44100;
for j=1:length(Key{1})

end
Player=audioplayer(Track./max(Track),fs);
play(Player);
pause(80);
end

%syntax de synt�!!!
function Note=Synthesizer(NotePitch,freqSampling,duration,NbOfHarmonics,type)

end